const nombre = document.getElementById("nombre");
const apellido = document.getElementById("apellido");
const pais = document.getElementById("pais");
const crear_email = document.getElementById("crear_email");
const crear_pass = document.getElementById("crear_pass");
const rep_pass = document.getElementById("rep_pass");


const div_ingresar = document.getElementById("iniciar-sesion");
const div_registrarse = document.getElementById("crear-cuenta");

let usuarios = [];


class Usuario{
    
    constructor (nombre,apellido,pais,email,password,rep_password) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.pais = pais;
        this.email = email;
        this.password = password;
        this.rep_password = rep_password;

    }

}
//nombre,apellido,pais,email,password,rep_password
function crear_cuenta(){
    const _nombre = nombre.value;
    const _apellido = apellido.value;
    const _pais = pais.value;
    const _email = crear_email.value;
    const _password = crear_pass.value;
    const _rep_password = rep_pass.value;

    crear_pass.classList.remove('error');
    pass.classList.remove('error');
    crear_email.classList.remove('error');
    div_registrarse.classList.remove('otro');

    if (_nombre !== "" && _apellido !== "" && _pais !== "" && _email !== "" && _password !== "" && _rep_password !== ""){
        

        if (_password === _rep_password){
            let ya_registrado = false;
            for (const usuario of usuarios) {
                
                if (usuario.email === _email ){
                    ya_registrado = true;
                }
            }

            if (ya_registrado === false || usuarios.length === 0)  {
                usuarios.push(new Usuario(_nombre,_apellido,_pais,_email,_password,_rep_password));
                console.log("Usuario creado con exito")
            } else if (ya_registrado === true){
                alert("El email ya esta registrado");
                crear_email.classList.add('error');
            }

        } else {
            alert("Las contraseñas deben coincidir");
            crear_pass.classList.add('error');
            rep_pass.classList.add('error');
        }    
        
    } else {
        alert('Complete todos los campos.')
        div_registrarse.classList.add('otro');
    }
}

const email = document.getElementById("email");
const pass = document.getElementById("pass");

function iniciar_sesion() {
    const _email = email.value;
    const _pass = pass.value;

    let habilitado = false;

    div_ingresar.classList.remove('otro')
    
    for (const usuario of usuarios) {
        if ( usuario.email === _email && usuario.password === _pass){
            habilitado = true;
        }    
    }
    
    if (habilitado){
        alert("Sesion iniciada con exito")
        div_ingresar.classList.remove('otro')
    } else {
        alert("Emaail o contraseña incorrecta")
        div_ingresar.classList.add('otro')
    }
}


const registrarse = document.getElementById('registrarse')
registrarse.addEventListener('click',crear_cuenta);


const ingresar = document.getElementById('ingresar')
ingresar.addEventListener('click',iniciar_sesion);